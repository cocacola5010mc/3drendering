export default class Triangle {
  // a, b, and c are vectors
  constructor(a, b, c) {
    this.a = a;
    this.b = b;
    this.c = c;
  }
}
